package net.htpu.boot.dao;

import net.htpu.boot.model.Example;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by david.han on 2016/9/29.
 */
@Mapper
public interface MybatisTestDao {
    @Select("select node_id nodeId, node_name nodeName from example")
    List<Example> findAllExamples();
}
