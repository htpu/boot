package net.htpu.boot.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by david.han on 2016/9/29.
 */
@Service
public class ArticleDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public Map<String, Object> loadSample() {
        return jdbcTemplate.queryForMap("select * from example limit 1");
    }
}
