package net.htpu.boot.controller;

import net.htpu.boot.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.Map;

/**
 * Created by david.han on 2016/9/29.
 */
@Controller
public class HelloController {
    @Autowired
    private ArticleService articleService;

    @RequestMapping("/hello")
    public String hello(Map<String, Object> model) {
        model.put("now", new Date());
        model.put("sample", articleService.loadSample());
        model.put("examples", articleService.findAllExamples());
        return "hello";
    }
}
