package net.htpu.boot.service;

import net.htpu.boot.model.Example;

import java.util.List;
import java.util.Map;

/**
 * Created by david.han on 2016/9/29.
 */
public interface ArticleService {
    Map<String, Object> loadSample();

    List<Example> findAllExamples();
}
