package net.htpu.boot.service.impl;

import net.htpu.boot.dao.ArticleDao;
import net.htpu.boot.dao.MybatisTestDao;
import net.htpu.boot.model.Example;
import net.htpu.boot.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by david.han on 2016/9/29.
 */
@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    private ArticleDao articleDao;

    @Autowired
    private MybatisTestDao mybatisTestDao;

    @Override
    public Map<String, Object> loadSample(){
        return articleDao.loadSample();
    }

    @Override
    public List<Example> findAllExamples(){
        return mybatisTestDao.findAllExamples();
    }
}
