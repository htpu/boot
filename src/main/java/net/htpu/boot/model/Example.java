package net.htpu.boot.model;

/**
 * Created by david.han on 2016/9/29.
 */
public class Example {
    private long nodeId;
    private String nodeName;

    public long getNodeId() {
        return nodeId;
    }

    public void setNodeId(long nodeId) {
        this.nodeId = nodeId;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    @Override
    public String toString() {
        return "Example{" +
                "nodeId=" + nodeId +
                ", nodeName='" + nodeName + '\'' +
                '}';
    }
}
